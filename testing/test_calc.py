import unittest

import calc

class TestCalc(unittest.TestCase):

    def test_add(self):
        self.assertEqual(calc.add(10,5), 15)
        self.assertEqual(calc.add(-1,1), 0)
        self.assertEqual(calc.add(-1,-1), -2)

    def test_substruct(self):
        self.assertEqual(calc.substruct(10,5), 5)
        self.assertEqual(calc.substruct(-1,1), -2)
        self.assertEqual(calc.substruct(-1,-1), 0)

    def test_multiply(self):
        self.assertEqual(calc.multiply(10,5), 50)
        self.assertNotEqual(calc.multiply(10,5), 15)
        self.assertEqual(calc.multiply(-1,1), -1)
        self.assertEqual(calc.multiply(-1,-1), 1)
    def test_devide(self):
        self.assertEqual(calc.devide(10,5), 2)
        self.assertEqual(calc.devide(-1,1), -1)
        self.assertEqual(calc.devide(-1,-1), 1)
        self.assertEqual(calc.devide(5,2), 2.5)

        with self.assertRaises(ValueError):
            calc.devide(10, 0)

if __name__ == "__main__":
# equivalent a la commande: python -m unittetest test_calc.py
    unittest.main()

