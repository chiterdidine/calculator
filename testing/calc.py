""" calc module"""
def add(x:int, y:int): 
    """this function compute the sum of two numbers
    Parameters
    x: int
    first operand
    y: int
    second operand

    Returns
    int
    sum of two operands
    """
    return x + y
def substruct(x, y):
    return x - y
def multiply(x, y):
    return x*y
def devide(x, y):
    if y == 0:
        raise ValueError("Can not devide by zero!")
    return x/y
